package com.hlhj.guanggou.result;

import com.hlhj.guanggou.po.Combination;
import com.hlhj.guanggou.po.Product;

public class CombinationResult {
	private Combination combinationinfo;
	private Product products;
	
	public Combination getCombinationinfo() {
		return combinationinfo;
	}
	public void setCombinationinfo(Combination combinationinfo) {
		this.combinationinfo = combinationinfo;
	}
	public Product getProducts() {
		return products;
	}
	public void setProducts(Product products) {
		this.products = products;
	}
	
	
}
